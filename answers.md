# Priority queue

## 1.1

### Where do processes need to be added to the queue?

`userinit()`
`fork()`

### Where do processes need to be removed from the queue?

`exit()`

## 1.2

```c
void userinit(void)
{
  struct proc *p;

  // aquire the priority lock
  acquire(&prio_lock);

  p = allocproc();
  initproc = p;

  // add to the priority queue
  insert_into_prio_queue(p);


  // allocate one user page and copy init's instructions
  // and data into it.
  uvminit(p->pagetable, initcode, sizeof(initcode));
  p->sz = PGSIZE;

  // prepare for the very first "return" from kernel to user.
  p->tf->epc = 0;     // user program counter
  p->tf->sp = PGSIZE; // user stack pointer

  safestrcpy(p->name, "initcode", sizeof(p->name));
  p->cmd = strdup("init");
  p->cwd = namei("/");
  p->state = RUNNABLE;

  release(&p->lock);
  release(&prio_lock);
}
```

```c
int fork(void)
{
  int i, pid;
  struct proc *np;
  struct proc *p = myproc();

  // Aquire the priority lock.
  acquire(&prio_lock);

  // Allocate process.
  if ((np = allocproc()) == 0)
  {
    release(&prio_lock);
    return -1;
  }

  // Copy user memory from parent to child.
  if (uvmcopy(p->pagetable, np->pagetable, p->sz) < 0)
  {
    freeproc(np);
    release(&np->lock);
    release(&prio_lock);
    return -1;
  }

  np->sz = p->sz;

  np->parent = p;

  // Adds the new process to the priority queue.
  np->priority = p->priority;
  insert_into_prio_queue(np);

  // copy saved user registers.
  *(np->tf) = *(p->tf);

  // Cause fork to return 0 in the child.
  np->tf->a0 = 0;

  // increment reference counts on open file descriptors.
  for (i = 0; i < NOFILE; i++)
    if (p->ofile[i])
      np->ofile[i] = filedup(p->ofile[i]);
  np->cwd = idup(p->cwd);

  safestrcpy(np->name, p->name, sizeof(p->name));
  np->cmd = strdup(p->cmd);
  pid = np->pid;

  np->state = RUNNABLE;

  release(&np->lock);
  release(&prio_lock);

  return pid;
}
```

```c
// Exit the current process.  Does not return.
// An exited process remains in the zombie state
// until its parent calls wait().
void exit(int status)
{
  struct proc *p = myproc();

  if (p == initproc)
    panic("init exiting");

  // Close all open files.
  for (int fd = 0; fd < NOFILE; fd++)
  {
    if (p->ofile[fd])
    {
      struct file *f = p->ofile[fd];
      fileclose(f);
      p->ofile[fd] = 0;
    }
  }

  begin_op(ROOTDEV);
  iput(p->cwd);
  end_op(ROOTDEV);
  p->cwd = 0;

  // we might re-parent a child to init. we can't be precise about
  // waking up init, since we can't acquire its lock once we've
  // acquired any other proc lock. so wake up init whether that's
  // necessary or not. init may miss this wakeup, but that seems
  // harmless.
  acquire(&initproc->lock);
  wakeup1(initproc);
  release(&initproc->lock);

  // grab a copy of p->parent, to ensure that we unlock the same
  // parent we locked. in case our parent gives us away to init while
  // we're waiting for the parent lock. we may then race with an
  // exiting parent, but the result will be a harmless spurious wakeup
  // to a dead or wrong process; proc structs are never re-allocated
  // as anything else.
  acquire(&prio_lock);
  acquire(&p->lock);
  struct proc *original_parent = p->parent;
  remove_from_prio_queue(p);
  release(&p->lock);
  release(&prio_lock);

  // we need the parent's lock in order to wake it up from wait().
  // the parent-then-child rule says we have to lock it first.
  acquire(&original_parent->lock);

  acquire(&p->lock);

  // Give any children to init.
  reparent(p);

  // Parent might be sleeping in wait().
  wakeup1(original_parent);

  p->xstate = status;
  p->state = ZOMBIE;

  release(&original_parent->lock);

  // Jump into the scheduler, never to return.
  sched();
  panic("zombie exit");
}
```

## 2

### 2.1

```c
// From the priority queue, get the next process to run. If found the process will be returned with it's lock acquired. This function also aquires the prio_lock.
struct proc *pick_highest_priority_runnable_proc(void)
{
  acquire(&prio_lock);

  for (struct list_proc **lp = prio; lp < &prio[NPRIO]; lp++)
  {
    for (struct list_proc *cursor = *lp; cursor; cursor = cursor->next)
    {
      struct proc *p = cursor->p;
      acquire(&p->lock);
      if (p->state == RUNNABLE)
      {
        return p;
      }
      release(&p->lock);
    }
  }

  // no runnable processes
  release(&prio_lock);

  return 0;
}
```

### 2.2

```c
// Slightly quicker than remove then insert.
// Requires the prio_lock to be held.
void move_proc_to_end_of_queue(struct proc *p)
{
  int prio_index = p->priority;
  struct list_proc *prev = 0;
  struct list_proc *plist = 0;

  for (struct list_proc *cursor = prio[prio_index]; cursor; cursor = cursor->next)
  {
    if (cursor->p == p)
    {
      if (prev == 0)
        prio[prio_index] = cursor->next;
      else
        prev->next = cursor->next;

      plist = cursor;
      continue;
    }
    prev = cursor;
  }

  // add to end of queue
  plist->next = 0;
  if (prev)
    prev->next = plist;
  else
    prio[prio_index] = plist;
}

// Per-CPU process scheduler.
// Each CPU calls scheduler() after setting itself up.
// Scheduler never returns.  It loops, doing:
//  - choose a process to run.
//  - swtch to start running that process.
//  - eventually that process transfers control
//    via swtch back to the scheduler.
void scheduler(void)
{
  struct proc *p;
  struct cpu *c = mycpu();

  c->proc = 0;
  for (;;)
  {
    // Avoid deadlock by giving devices a chance to interrupt.
    intr_on();

    // Run the for loop with interrupts off to avoid
    // a race between an interrupt and WFI, which would
    // cause a lost wakeup.
    intr_off();

    if ((p = pick_highest_priority_runnable_proc()))
    {
      // found a process

      // Sets it's state to RUNNING
      p->state = RUNNING;
      c->proc = p;

      // // Move the process to the end of the queue
      move_proc_to_end_of_queue(p);

      release(&prio_lock);

      swtch(&c->scheduler, &p->context);

      // Process is done running for now.
      // It should have changed its p->state before coming back.
      c->proc = 0;

      // ensure that release() doesn't enable interrupts.
      // again to avoid a race between interrupt and WFI.
      c->intena = 0;

      release(&p->lock);
    }
    else
    {
      release(&prio_lock);

      asm volatile("wfi");
    }
  }
}

void scheduler_OLD(void)
// keep the old scheduler for now.
...
```

### 3.1

```c
int nice(int pid, int priority){
  return 0;
}

int nice(int pid, int priority);
```

### 3.2

```c
// sysproc.c
uint64
sys_nice(void)
{
  int pid;
  int priority;

  if (argint(0, &pid) < 0)
    return -1;

  if (argint(1, &priority) < 0)
    return -1;

  if (priority < 0 || priority >= NPRIO)
    return -1;

  return nice(pid, priority);
  return 0;
}
```

### 3.3

```c
int nice(int pid, int priority)
{
  struct proc *p;

  for (p = proc; p < &proc[NPROC]; p++)
  {
    acquire(&prio_lock);
    acquire(&p->lock);
    if (p->pid == pid)
    {
      // removes the process from the current priority queue
      remove_from_prio_queue(p);

      // sets the priority of the process to the given priority
      p->priority = priority;

      // adds the process to the correct priority queue
      insert_into_prio_queue(p);

      release(&p->lock);
      release(&prio_lock);
      return 1;
    }
    release(&p->lock);
    release(&prio_lock);
  }
  return 0;
}
```

### 3.4

![](3.4.png)

`nice 10 7`

# 4

### 4.1

```bash
$ mutest
Fils, tu m'attendras
Fils, tu m'attendras
Fils, tu m'attendras
Fils, tu m'attendras
Fils, tu m'attendras
Fils, tu m'attendras
Fils, tu m'attendras
Fils, tu m'attendras
Fils, tu m'attendras
Fils, tu m'attendras
Fils, tu m'attendras
Fils, tu m'attendras
Oui, père.
Fils, tu m'attendras
Fils, tu m'attendras
Fils, tu m'attendras
Fils, tu m'attendras
Fils, tu m'attendras
Fils, tu m'attendras
Fils, tu m'attendras
Fils, tu m'attendras
Fils, tu m'attendras
Fils, tu m'attendras
Fils, tu m'attendras
```

`Oui père` should be at the end.

```bash
$ mutest2
Result = 50
```

### 4.3

EZ

### 4.4

```c

```
